package br.com.ioasys.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ioasys.api.model.FilmeModel;

@Repository
public interface FilmeRepository extends JpaRepository<FilmeModel, Integer> {

	Optional<FilmeModel> findById(Integer id);

	@Query(value = "SELECT * FROM Filme f WHERE f.nome = :nome", nativeQuery = true)
	Optional<FilmeModel> findByNome(@Param("nome") String nome);

	@Query(value = "SELECT * FROM Filme f WHERE f.genero = :genero", nativeQuery = true)
	Optional<FilmeModel> findByGenero(@Param("genero") String genero);

	@Query(value = "SELECT * FROM Filme f WHERE f.diretor = :diretor", nativeQuery = true)
	Optional<FilmeModel> findByDiretor(@Param("diretor") String diretor);

}
