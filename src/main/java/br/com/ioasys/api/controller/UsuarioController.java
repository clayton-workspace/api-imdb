package br.com.ioasys.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.api.model.AvaliacaoModel;
import br.com.ioasys.api.model.FilmeModel;
import br.com.ioasys.api.model.UsuarioModel;
import br.com.ioasys.api.repository.AvaliacaoRepository;
import br.com.ioasys.api.repository.FilmeRepository;
import br.com.ioasys.api.repository.UsuarioRepository;
import br.com.ioasys.api.services.UsuarioService;

@RestController
@RequestMapping("/usuario")
public class UsuarioController extends UsuarioService {

	@Autowired
	private final AvaliacaoRepository avaliacaoRepository;
	@Autowired
	private final FilmeRepository filmeRepository;
	@Autowired
	private final UsuarioRepository usuarioRepository;
	private final PasswordEncoder encoder;

	public UsuarioController(FilmeRepository filmeRepository, AvaliacaoRepository avaliacaoRepository,
			UsuarioRepository usuarioRepository, PasswordEncoder encoder) {
		this.filmeRepository = filmeRepository;
		this.avaliacaoRepository = avaliacaoRepository;
		this.usuarioRepository = usuarioRepository;
		this.encoder = encoder;
	}

	@PostMapping("{filmeId}/classificar-filme")
	public ResponseEntity<AvaliacaoModel> classificarFilme(@PathVariable("filmeId") Integer filmeId,
			@RequestBody Integer nota) {
		FilmeModel filme = filmeRepository.findById(filmeId).get();
		if (filme == null)
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

		AvaliacaoModel avaliacao = avaliarFilme(filme, nota);

		return ResponseEntity.ok(avaliacaoRepository.save(avaliacao));
	}

	@PutMapping(value = "/editar")
	public ResponseEntity<UsuarioModel> editar(@PathVariable("id") Integer id, @RequestBody UsuarioModel usuario) {
		return usuarioRepository.findById(usuario.getId()).map(r -> {
			r.setEmail(usuario.getEmail());
			r.setPassword((encoder.encode(usuario.getPassword())));
			UsuarioModel editado = usuarioRepository.save(r);
			return ResponseEntity.ok().body(editado);
		}).orElse(ResponseEntity.notFound().build());
	}

	@PutMapping(value = "/excluir")
	public ResponseEntity<UsuarioModel> excluir(@PathVariable("id") Integer id, @RequestBody UsuarioModel usuario) {
		return usuarioRepository.findById(usuario.getId()).map(u -> {
			u.setAuthorities("false");
			UsuarioModel editado = usuarioRepository.save(u);
			return ResponseEntity.ok().body(editado);
		}).orElse(ResponseEntity.notFound().build());
	}

	@GetMapping("/listar-filmes")
	public ResponseEntity<?> listarFilmes(Pageable pageable) {
		return ResponseEntity.ok(filmeRepository.findAll(pageable));
	}

	@GetMapping("/buscar-nome-filme")
	public ResponseEntity<?> buscarPorNomeFilme(String nome) {
		return ResponseEntity.ok(filmeRepository.findByNome(nome));
	}

	@GetMapping("/buscar-genero-filme")
	public ResponseEntity<?> buscarPorGenero(String genero) {
		return ResponseEntity.ok(filmeRepository.findByGenero(genero));
	}

	@GetMapping("/buscar-diretor-filme")
	public ResponseEntity<?> buscarPorDiretor(String diretor) {
		return ResponseEntity.ok(filmeRepository.findByDiretor(diretor));
	}
}