package br.com.ioasys.api.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.api.model.UsuarioModel;
import br.com.ioasys.api.repository.UsuarioRepository;

@RestController
@RequestMapping("/")
public class VisitanteController {
	
	@Autowired
	private final UsuarioRepository usuarioRepository;
	private final PasswordEncoder encoder;

	public VisitanteController(UsuarioRepository usuarioRepository, PasswordEncoder encoder) {
		this.usuarioRepository = usuarioRepository;
		this.encoder = encoder;
	}

	@PostMapping("/cadastrar")
	public ResponseEntity<UsuarioModel> salvar(@RequestBody UsuarioModel usuario) {
		usuario.setPassword(encoder.encode(usuario.getPassword()));
		return ResponseEntity.ok(usuarioRepository.save(usuario));
	}

	@GetMapping("/validarSenha")
	public ResponseEntity<Boolean> validarSenha(@RequestParam String login, @RequestParam String password) {
		Optional<UsuarioModel> optUsuario = usuarioRepository.findByLogin(login);
		if (optUsuario.isEmpty()) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(false);
		}

		UsuarioModel usuario = optUsuario.get();
		boolean valid = encoder.matches(password, usuario.getPassword());

		HttpStatus status = (valid) ? HttpStatus.OK : HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body(valid);
	}
}