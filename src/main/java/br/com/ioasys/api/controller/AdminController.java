package br.com.ioasys.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.api.model.FilmeModel;
import br.com.ioasys.api.model.UsuarioModel;
import br.com.ioasys.api.repository.FilmeRepository;
import br.com.ioasys.api.repository.UsuarioRepository;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private final UsuarioRepository usuarioRepository;
	@Autowired
	private final FilmeRepository filmeRepository;
	private final PasswordEncoder encoder;

	public AdminController(FilmeRepository filmeRepository, UsuarioRepository usuarioRepository,
			PasswordEncoder encoder) {
		this.filmeRepository = filmeRepository;
		this.usuarioRepository = usuarioRepository;
		this.encoder = encoder;
	}

	@PostMapping("/cadastrar")
	public ResponseEntity<UsuarioModel> salvar(@RequestBody UsuarioModel usuario) {
		usuario.setPassword(encoder.encode(usuario.getPassword()));
		return ResponseEntity.ok(usuarioRepository.save(usuario));
	}

	@PostMapping("/cadastrar-filme")
	public ResponseEntity<FilmeModel> salvar(@RequestBody FilmeModel filme) {
		return ResponseEntity.ok(filmeRepository.save(filme));
	}

	@PutMapping("/editar/{id}")
	public ResponseEntity<UsuarioModel> editar(@PathVariable("id") Integer id, @RequestBody UsuarioModel usuario) {
		return usuarioRepository.findById(id).map(r -> {
			r.setNome(usuario.getNome());
			r.setEmail(usuario.getEmail());
			r.setPassword((encoder.encode(usuario.getPassword())));
			r.setAuthorities(usuario.getAuthorities());
			UsuarioModel editado = usuarioRepository.save(r);
			return ResponseEntity.ok().body(editado);
		}).orElse(ResponseEntity.notFound().build());
	}

	@PutMapping("/excluir/{id}")
	public ResponseEntity<UsuarioModel> excluir(@PathVariable("id") Integer id, @RequestBody UsuarioModel usuario) {
		return usuarioRepository.findById(id).map(u -> {
			u.setStatus("false");
			UsuarioModel editado = usuarioRepository.save(u);
			return ResponseEntity.ok().body(editado);
		}).orElse(ResponseEntity.notFound().build());
	}

	@GetMapping("/listar-usuarios")
	public ResponseEntity<?> listarUsuarios(Pageable pageable) {
		return ResponseEntity.ok(usuarioRepository.findAll(pageable));
	}

}