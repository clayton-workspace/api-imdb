package br.com.ioasys.api.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.ioasys.api.data.DetalheUsuarioData;
import br.com.ioasys.api.model.UsuarioModel;
import br.com.ioasys.api.repository.UsuarioRepository;

public class JWTAutenticarFilter extends UsernamePasswordAuthenticationFilter {

	public static final int TOKEN_EXPIRACAO = 600_000;
	public static final String TOKEN_SENHA = "cff37da8-88ce-4ce3-a4c9-4e7f3aee5605";

	private final AuthenticationManager authenticationManager;
	private UsuarioRepository usuarioRepository;

	public JWTAutenticarFilter(AuthenticationManager authenticationManager, UsuarioRepository usuarioRepository) {
		this.authenticationManager = authenticationManager;
		this.usuarioRepository = usuarioRepository;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			UsuarioModel usuario = new ObjectMapper().readValue(request.getInputStream(), UsuarioModel.class);

			UsuarioModel usuarioModel = usuarioRepository.findByLogin(usuario.getLogin()).get();

			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(usuario.getLogin(),
					usuario.getPassword(), usuarioModel.getAuthoritiesList()));
		} catch (IOException e) {
			throw new RuntimeException("Falha ao autenticar usuario", e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {

		DetalheUsuarioData usuarioData = (DetalheUsuarioData) authResult.getPrincipal();

		String token = JWT.create().withSubject(usuarioData.getUsername())
				.withExpiresAt(new Date(System.currentTimeMillis() + TOKEN_EXPIRACAO))
				.sign(Algorithm.HMAC512(TOKEN_SENHA));

		response.getWriter().write(token);
		response.getWriter().flush();
	}

}
