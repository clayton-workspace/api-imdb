package br.com.ioasys.api.security;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import br.com.ioasys.api.repository.UsuarioRepository;
import br.com.ioasys.api.services.DetalheUsuarioServiceImpl;

@EnableWebSecurity
public class JWTConfiguracao extends WebSecurityConfigurerAdapter {

	private final DetalheUsuarioServiceImpl usuarioService;
	private final PasswordEncoder passwordEncoder;
	private UsuarioRepository usuarioRepository;

	public JWTConfiguracao(DetalheUsuarioServiceImpl usuarioService, PasswordEncoder passwordEncoder,
			UsuarioRepository usuarioRepository) {
		this.usuarioService = usuarioService;
		this.passwordEncoder = passwordEncoder;
		this.usuarioRepository = usuarioRepository;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(usuarioService).passwordEncoder(passwordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()

				
				  .antMatchers(HttpMethod.POST, "/admin/**").hasRole("ADMIN")
				  .antMatchers(HttpMethod.GET, "/admin/**").hasRole("ADMIN")
				  .antMatchers(HttpMethod.PUT, "/admin/**").hasRole("ADMIN")
				  
				  .antMatchers(HttpMethod.GET, "/usuario/**").hasRole("USER")
				  .antMatchers(HttpMethod.PUT, "/usuario/**").hasRole("USER")
				 

				.antMatchers(HttpMethod.POST, "/**").permitAll().antMatchers(HttpMethod.GET, "/**").permitAll()
				.antMatchers(HttpMethod.PUT, "/**").permitAll()

				.anyRequest().authenticated().and()
				.addFilter(new JWTAutenticarFilter(authenticationManager(), usuarioRepository))
				.addFilter(new JWTValidarFilter(authenticationManager(), usuarioRepository)).sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

		CorsConfiguration corsConfiguration = new CorsConfiguration().applyPermitDefaultValues();
		source.registerCorsConfiguration("/**", corsConfiguration);

		return source;
	}

}
