package br.com.ioasys.api.data;

import java.util.Collection;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.ioasys.api.model.UsuarioModel;

public class DetalheUsuarioData implements UserDetails {

	private final Optional<UsuarioModel> usuario;

	public DetalheUsuarioData(Optional<UsuarioModel> usuario) {
		this.usuario = usuario;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return usuario.orElse(new UsuarioModel()).getAuthoritiesList();
	}

	@Override
	public String getPassword() {
		return usuario.orElse(new UsuarioModel()).getPassword();
	}

	@Override
	public String getUsername() {
		return usuario.orElse(new UsuarioModel()).getLogin();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// return Boolean.valueOf(usuario.orElse(new UsuarioModel()).getStatus());
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
