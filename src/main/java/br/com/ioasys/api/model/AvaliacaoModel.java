package br.com.ioasys.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "filmeClassificacao")

public class AvaliacaoModel {

	public AvaliacaoModel(UsuarioModel usuario, FilmeModel filme, @NotNull Integer voto) {
		this();
		this.usuario = usuario;
		this.filme = filme;
		this.voto = voto;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne
	@JsonIgnore
	private UsuarioModel usuario;

	@ManyToOne
	@JsonIgnore
	private FilmeModel filme;

	@NotNull
	@Min(0)
	@Max(5)
	private Integer voto;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UsuarioModel getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioModel usuario) {
		this.usuario = usuario;
	}

	public FilmeModel getFilme() {
		return filme;
	}

	public void setFilme(FilmeModel filme) {
		this.filme = filme;
	}

	public Integer getVoto() {
		return voto;
	}

	public void setVoto(Integer voto) {
		this.voto = voto;
	}

	private AvaliacaoModel() {

	}
}
