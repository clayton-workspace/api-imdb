package br.com.ioasys.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.ioasys.api.model.AvaliacaoModel;
import br.com.ioasys.api.model.FilmeModel;
import br.com.ioasys.api.model.UsuarioModel;
import br.com.ioasys.api.repository.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired
	private UsuarioRepository usuarioRepository;

	public AvaliacaoModel avaliarFilme(FilmeModel filme, Integer nota) {
		UsuarioModel usuario = getUserByToken();

		if (usuario.getAuthoritiesList().stream().anyMatch(role -> role.equals("ROLE_ADMIN")))
			throw new IllegalAccessError("Somente usuário comum pode votar.");

		AvaliacaoModel avaliacao = new AvaliacaoModel(usuario, filme, nota);

		return avaliacao;
	}

	public UsuarioModel getUserByToken() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String login = (String) authentication.getPrincipal();

		Optional<UsuarioModel> opt = usuarioRepository.findByLogin(login);
		if (opt.isEmpty())
			throw new IllegalStateException("Usuario inexistente.");

		return opt.get();
	}
}
