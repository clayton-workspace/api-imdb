package br.com.ioasys.api.services;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.com.ioasys.api.data.DetalheUsuarioData;
import br.com.ioasys.api.model.UsuarioModel;
import br.com.ioasys.api.repository.UsuarioRepository;

@Component
public class DetalheUsuarioServiceImpl implements UserDetailsService {

	private final UsuarioRepository repository;

	public DetalheUsuarioServiceImpl(UsuarioRepository repository) {
		this.repository = repository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<UsuarioModel> usuario = repository.findByLogin(username);

		if (usuario.isEmpty())
			throw new UsernameNotFoundException("Usuário [" + username + "] não encontrado");

		return new DetalheUsuarioData(usuario);
	}
}
