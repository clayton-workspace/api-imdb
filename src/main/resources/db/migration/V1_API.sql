
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for filme
-- ----------------------------
DROP TABLE IF EXISTS `filme`;
CREATE TABLE `filme`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atores` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `diretor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `genero` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nome` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `resumo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of filme
-- ----------------------------
INSERT INTO `filme` VALUES (1, 'Neymar', 'Claytinho', 'Comedia', 'Nome do Filme', 'Tafareeeeeeeeeeel');
INSERT INTO `filme` VALUES (2, 'Mickael, Manon, Azem Camara', 'Charles Van Tieghem', 'Comedia', 'O Melhor amigo', 'Charles Van Tieghem');
INSERT INTO `filme` VALUES (3, 'Jorge, Debby, Ryan', 'Adam Randall', 'Suspense', 'passageiras', 'Um jovem motorista leva duas mulheres misteriosas para uma noite de festa.');
INSERT INTO `filme` VALUES (4, 'Jim, Kevin, Kate', 'Robert Luketic', 'Dramas', 'Quebrando a banca', 'Um brilhante grupo de alunos se especializa em contar cartas, pensando em faturar milhoes jogando Blackjack nos cassinos de Las Vegas.');
INSERT INTO `filme` VALUES (8, 'Jim, Kevin, Kate', 'Robert Luketic', 'Dramas', 'Quebrando a banca 2', 'Um brilhante grupo de alunos se especializa em contar cartas, pensando em faturar milhoes jogando Blackjack nos cassinos de Las Vegas.');

-- ----------------------------
-- Table structure for filme_classificacao
-- ----------------------------
DROP TABLE IF EXISTS `filme_classificacao`;
CREATE TABLE `filme_classificacao`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voto` int(11) NOT NULL,
  `filme_id` int(11) NULL DEFAULT NULL,
  `usuario_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKob1gr0e9nf416ktw2k13kmcvm`(`filme_id`) USING BTREE,
  INDEX `FKofsvi2y0kvngt3na6vo64jhwu`(`usuario_id`) USING BTREE,
  CONSTRAINT `FKob1gr0e9nf416ktw2k13kmcvm` FOREIGN KEY (`filme_id`) REFERENCES `filme` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FKofsvi2y0kvngt3na6vo64jhwu` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of filme_classificacao
-- ----------------------------

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorities` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `login` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nome` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `UK_pm3f4m4fqv89oeeeac4tbe2f4`(`login`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES (1, 'ROLE_USER', 'claytonhm06@gmail.com', 'clayton', 'Clayton Henrique Magalhaes', '$2a$10$.ZYHD4BBDKjlUkj1UqnuKeHknQD3dA65O5UljCCtcH6oh2ckQN4wC', 'true');
INSERT INTO `usuario` VALUES (2, 'ROLE_USER,ROLE_ADMIN', 'claytonhm06@gmail.com', 'admclayton', 'Clayton Henrique', '$2a$10$MEEUmAK9OshFYZ42Yl51/.V.wmIEvXyC/o5Yg6n7yLtasnb6FZm3a', 'true');
INSERT INTO `usuario` VALUES (3, 'ROLE_USER', 'claytonhm06@gmail.com', 'Amanda', 'Amanda', '$2a$10$fGAlnPVHq36aAPR7ptdHiO78YW7LEHX0E1xuOCwGUrJ2C2Wh/9vhW', 'true');
INSERT INTO `usuario` VALUES (4, 'ROLE_USER', 'barbara@gmail.com', 'Barbara', 'barbara', '$2a$10$P9e4g7FjFkTUZrHSzEAMleWhBQS4CHrIviPTts9qzZGALAHcxpQOO', 'true');
INSERT INTO `usuario` VALUES (6, 'ROLE_USER', 'Karol@gmail.com', 'Karol', 'Karol', '$2a$10$WUcbirjp/jQuUsluY2F/XuiFRIF1QYdsb3CmRCDzs2/uekxHkXZNm', 'true');

SET FOREIGN_KEY_CHECKS = 1;
